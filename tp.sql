DROP DATABASE IF EXIST TPBD2;
CREATE DATABASE TPBD2;
/c TPBD2

create table buferpool(
    nro_frame    int,
    free            boolean,
    dirty           boolean,
    nro_disk_page   int,
    last_touch      timestamp

) ;

--Funciòn que retorna numero de frame.
create or replace function get_disk_page (nro_page int) returns integer as $$

$$language plpgsql;

create or replace function pick_frame_LRU () returns integer as $$

$$language plpgsql;

create or replace function pick_frame_MRU () returns integer as $$

$$language plpgsql;


